# num_sdk_msgs

This repo contains the public API for the Numurus ROS SDK. All message and service definitions that are exposed to external applications should be contained in this repo.

